# TP RealWorld

Créer un nouveau paragraphe pour chaque section 6.x.y

## 6.3

### 6.3.1

SHA1 : 31b96deef6ee1f4d3dd876f8877411e2fc471ecc 

### 6.3.2

https://github.com/mkenney/docker-npm/blob/master/node-8-alpine/Dockerfile
docker image pull mkenney/npm:node-8-debian

### 6.3.3

container run -ti $(pwd):/src mkenney/npm:node-8-debian npm install
docker container run -ti -p000:8080 -v $(pwd):/src mkenney/npm:node-8-debian npm run dev

## 6.4

8df1e97658c7ececf9d6b9fc6393463f90f0eb9f

### 6.4.2 

docker image pull gradle:4.7.0-jdk8-alpine
f438b7d58d0a 
/home/gradle/.gradle

### 6.4.3

docker volume create gradle-home
docker volume ls -a

gradle              4.7.0-jdk8-alpine   f438b7d58d0a        3 weeks ago         187MB
mkenney/npm         node-8-debian       9af19a7f4e2c        3 weeks ago         702MB

### 6.4.4 

docker container run -ti -v gradle-home:/home/gradle/.gradle -v $(pwd):/src -w=/src gradle:4.7.0-jdk8-alpine gradle build

### 6.4.5

docker container run -ti -p 8001:8080 -v gradle-home:/home/gradle/.gradle -v $(pwd):/src -w=/src gradle:4.7.0-jdk8-alpine gradle bootRun
articles	[]
articlesCount	0

## 6.5

### 6.5.1

articles	
0	
id	"0c2554b0-91ce-42cc-ab59-8370f9cf72e1"
slug	"gné"
title	"Gné"
description	"gné"
body	"gné"
favorited	false
favoritesCount	0
createdAt	"2018-05-16T13:42:00.443Z"
updatedAt	"2018-05-16T13:42:00.443Z"
tagList	[]
author	
username	"David"
bio	""
image	"https://static.productionready.io/images/smiley-cyrus.jpg"
following	false
articlesCount	1

## 6.6

docker container run -ti -v $(pwd):/src mkenney/npm:node-8-debian npm run build

### 6.6.2

docker container run -v $(pwd)/dist:/usr/nginx/html -p 8000:80 nginx:alpine


